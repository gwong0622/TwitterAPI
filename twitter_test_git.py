import os
import oauth2 as oauth
import urllib2 as urllib
import json
import unicodedata
import operator

#You need to sign up and create an application at https://dev.twitter.com/apps
#to create the access_token_key, access_token_secret, consumer key and consumer secret

access_token_key = "ACCESS_TOKEN"
access_token_secret = "ACCESS_TOKEN_SECRET"

consumer_key = "CONSUMER_KEY"
consumer_secret = "CONSUMER_SECRET"


_debug = 0

oauth_token    = oauth.Token(key=access_token_key, secret=access_token_secret)
oauth_consumer = oauth.Consumer(key=consumer_key, secret=consumer_secret)

signature_method_hmac_sha1 = oauth.SignatureMethod_HMAC_SHA1()

http_method = "GET"


http_handler  = urllib.HTTPHandler(debuglevel=_debug)
https_handler = urllib.HTTPSHandler(debuglevel=_debug)

'''
Construct, sign, and open a twitter request
using the hard-coded credentials above.
'''
def twitterreq(url, method, parameters):
    req = oauth.Request.from_consumer_and_token(oauth_consumer,
                                             token=oauth_token,
                                             http_method=http_method,
                                             http_url=url, 
                                             parameters=parameters)
    req.sign_request(signature_method_hmac_sha1, oauth_consumer, oauth_token)
    headers = req.to_header()
    if http_method == "POST":
        encoded_post_data = req.to_postdata()
    else:
        encoded_post_data = None
    url = req.to_url()

    opener = urllib.OpenerDirector()
    opener.add_handler(http_handler)
    opener.add_handler(https_handler)

    response = opener.open(url, encoded_post_data)
    return response

#read dictionary file with scores for sentiments
afinnfile = open("afinn-111.txt")
scores = {} # initialize an empty dictionary
for line in afinnfile:
    term, score  = line.split("\t")  # The file is tab-delimited. "\t" means "tab character"
    scores[term] = float(score)
    
#intialise sentiment scores
sentiment_score = [ ]
senttw = 0
i = 0

url = "https://api.twitter.com/1.1/search/tweets.json?q=Coventry%20University&result_type=recent"
parameters = []
response = twitterreq(url, "GET", parameters)
tweets = [ ]

for tweet in response:
    tw = json.loads(tweet.strip())
    for tw_node in tw["statuses"]:
        strdata=unicodedata.normalize("NFKD", tw_node["text"]).encode("ascii","ignore")
        terms = strdata.lower().encode('utf-8').split()
        for term in terms:
            if scores.has_key(term):
                senttw = senttw + scores[term]
        sentiment_score.append(senttw)
        tweets.append(tw_node["text"])
        #print tw_node["text"], ' Sentiment is ',sentiment_score[i]
        i = i +1
        senttw = 0

#sort by highest score
x = sorted(enumerate(sentiment_score), key=operator.itemgetter(1), reverse = True)    

for i in range(0,6):
   print tweets[x[i][0]], ' Sentiment is ', x[i][1],'\n'  
